# Quelques requêtes utiles


### Afficher les ingrédients proches d'un ingrédient donné

```
MATCH (n {name: "Citron"})-->(m)
RETURN m.name
```

### Définir un graphe à partir de données de la base

Créer un objet "graphe"

```
CALL gds.graph.create("graph", "ingredient", "ASSOCIE")
```

### Calcul de similarités entre les noeuds

A partir du graphe créé précédemment

```
CALL gds.nodeSimilarity.stream("graph")
YIELD node1, node2, similarity
RETURN gds.util.asNode(node1).name AS Ingredient1, gds.util.asNode(node2).name AS Ingredient2, similarity
ORDER BY similarity DESCENDING, Ingredient1, Ingredient2
```

### Regrouper des noeuds entre eux (clustering)

A partir du graphe créé précédemment

```
CALL gds.louvain.stream(
  "graph"
)
YIELD nodeId, communityId
RETURN gds.util.asNode(nodeId).name AS name, communityId
```
